def call(Map pipelineParams) {
    node{
        def projName = pipelineParams.ProjectName
        dir('config_files'){
            echo "Cloning config files using hardcoded development branch"
            git branch: 'main', credentialsId: 'bf638d1b-6de0-4044-9270-5c5551e1b3b8', url: 'https://gitlab.com/AboobakerSiddiq/poc-jenkins-library.git'
        }
        def props = readJSON file: "${env.WORKSPACE}/config_files/config/config.json" 
        //def SECRET = credentials('docker-cred')
        def gitUrl = props[projName].git_url
        def dockerUserId = props['common'].docker_userid
        def dockerRepo = props['common'].docker_repo
        def dockerImageName = dockerUserId+'/' + dockerRepo + ':' + "${env.BUILD_NUMBER}"

        stage('Build'){
            def branchName = input(
                id: 'branchInput',
                message: 'Select or enter the branch to build:',
                parameters: [
                    choice(
                        name: 'BRANCH',
                        choices: ['main', 'develop', 'feature/1'],
                        description: 'Select a predefined branch'
                    ),
                    string(
                        name: 'CUSTOM_BRANCH',
                        defaultValue: '',
                        description: 'Enter a custom branch name'
                    )
                ]
            )

            // Determine the selected branch
            def selectedBranch
            if (branchName.CUSTOM_BRANCH) {
                selectedBranch = branchName.CUSTOM_BRANCH
            } else {
                selectedBranch = branchName.BRANCH
            }
            
            git credentialsId: 'bf638d1b-6de0-4044-9270-5c5551e1b3b8', url: gitUrl, branch: selectedBranch

            withCredentials([string(credentialsId: 'docker-cred', variable: 'SECRET')]) {
                sh "docker build -t ${dockerImageName} ."
                sh "docker login -u ${dockerUserId} -p '${SECRET}'"
                sh "docker push ${dockerImageName}"
                sh "docker rmi ${dockerImageName} --force"
            }
        }
        stage('Development') {
            input (message: 'Approve the release for Development Env', ok: 'Submit', submitter: 'admin')

            echo 'Hello from Dev Stage'
        }
    }
    
        

}
