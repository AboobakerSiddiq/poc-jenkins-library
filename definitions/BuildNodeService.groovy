class BuildNodeService {

    Map<String, String> nodeBuildServices(Map pipelineParams) {
        String projName = pipelineParams.ProjectName
        String serverType = pipelineParams.ServerType
        Map props = readJSON(file: "${env.WORKSPACE}/config_files/config/config.json")
        String selectedBranch
        String gitUrl = props[projName].git_url
        String dockerUserId = props['common'].docker_userid
        String dockerRepo = props['common'].docker_repo
        String dockerImageName = "${dockerUserId}/${dockerRepo}:${env.BUILD_NUMBER}"
        String dockerImageArgument = serverType != null ?
            (serverType == 'HTTP' ? '-f Http.Dockerfile' : (serverType == 'Socket' ? '-f Socket.Dockerfile' : '')) : ''

        String branchName = input(
            id: 'branchInput',
            message: 'Select or enter the branch to build:',
            parameters: [
                choice(
                    name: 'BRANCH',
                    choices: ['main', 'develop', 'feature'],
                    description: 'Select a predefined branch'
                ),
                string(
                    name: 'CUSTOM_BRANCH',
                    defaultValue: '',
                    description: 'Enter a custom branch name'
                )
            ]
        )
        // Determine the selected branch
        selectedBranch = branchName.CUSTOM_BRANCH ?: branchName.BRANCH

        withCredentials([
            string(credentialsId: 'd79e18b4-0eaa-4446-a65f-fcf634dd527a', variable: 'GIT_CREDENTIALS'),
            string(credentialsId: 'docker-cred', variable: 'SECRET')
        ]) {
            git credentialsId: 'd79e18b4-0eaa-4446-a65f-fcf634dd527a', url: gitUrl, branch: selectedBranch
            // Build the image
            sh "sudo docker build -t ${dockerImageName} ."

            // Push the Image
            echo 'INFO - Push Docker Image to Docker Hub -- Starting.....'
            sh "docker login -u ${dockerUserId} -p ${SECRET}"
            sh "docker push ${dockerImageName}"
            echo 'INFO - Push Docker Image to Docker Hub Starting.....'

            // Remove the image from Jenkins server
            sh "sudo docker rmi --force ${dockerImageName}"
        }

        return [
            dockerImageName: dockerImageName,
            dockerContainerName: dockerContainerName
        ]
    }

}
