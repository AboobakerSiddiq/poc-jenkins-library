class ReleaseNodeService {

    void releaseNodeServices(Map pipelineParams, String dockerImageName, String dockerContainerName, String serverName) {
        String projName = pipelineParams.ProjectName
        Map props = readJSON file: "${env.WORKSPACE}/config_files/config/config.json"
        String logDir = props[projName].appLog
        String hostDir = props['common'].hostAddrLog
        String dockerVolume = logDir == '' ? '' : "-v ${hostDir}:${logDir}"
        String volumeBind = props[projName].volumeBind
        String memoryLimitAllowed = props['common'].memoryLimitAllowed
        String memorySize = (memoryLimitAllowed == 'YES' && props[projName].memory != '0') ?
        "--memory=${props[projName].memory}" : ''
        String nodeEnv = props[projName].nodeEnv
        String pythonEnv = props[projName].pythonEnv
        String enableSonarCubeAnalysis = props['common'].enableSonarCubeAnalysis
        String isFuncTestRequired = props[projName].isFuncTestRequired
        String enableTryMe = props['common'].enableTryMe

        withCredentials([usernamePassword(credentialsId: 'kea-devops',
        usernameVariable: 'dockerUsername', passwordVariable: 'dockerPassword')]) {
            stage('Running the docker container') {
                withCredentials([[$class: 'AmazonWebServicesCredentialsBinding',
                credentialsId: awsCredentials, accessKeyVariable: 'awsAccessKeyId',
                secretKeyVariable: 'awsSecretAccessKey']]) {
                    sshPublisher(publishers: [
                        sshPublisherDesc(configName: serverName, verbose: true, transfers: [
                            sshTransfer(
                                execCommand: """
                                    echo "Deleting old containers"
                                    docker stop ${dockerContainerName}
                                    docker rm ${dockerContainerName}
                                    echo "Deleted all running containers"
                                    echo "Deleting old images"
                                    docker rmi --force ${dockerImageName}
                                    echo "Image deleted"

                                    echo "Logging into Docker Hub"
                                    docker login -u $dockerUsername -p $dockerPassword
                                    echo "Logged in"

                                    echo "Pulling Docker image: $dockerImageName"
                                    docker pull ${dockerImageName}
                                    echo "Image pulled"

                                    echo "Deploying Docker container"
                                    sudo docker run ${memorySize} ${dockerVolume} ${volumeBind} -e NODE_ENV=${nodeEnv} -e PYTHON_ENV=${pythonEnv} -e AWS_ACCESS_KEY_ID=${awsAccessKeyId} -e AWS_SECRET_ACCESS_KEY=${awsSecretAccessKey} -e AWS_DEFAULT_REGION=ap-south-1 --network=host -d --restart=unless-stopped --name ${dockerContainerName} -it ${dockerImageName}"
                                    echo "Docker container deployed"
                                """, execTimeout: 120000
                            )
                        ])
                    ])
                }
            }
        }

        // Regression Testing
        if (enableSonarCubeAnalysis == 'YES') {
            steps {
                withSonarQubeEnv('SonarQube') {
                    sh 'npm install'
                    sh 'npm run sonar'
                }
            }
        }

        // Need to check the environment.
        if (isFuncTestRequired == 'YES' && ENV == 'psKeaQA') {
            echo 'Running JMeter test script...'
            echo 'Removing existing folders'
            echo 'Started to run JMeter test script'
            sh "mkdir ${env.WORKSPACE}/${env.BUILD_NUMBER}"
            sh "mkdir ${env.WORKSPACE}/${env.BUILD_NUMBER}/testReport"
            Map jmxFileForRegressionSuite = ENV == 'psKeaDEV' ?
            "${env.WORKSPACE}/config_files/jmeter/jmxFiles/sales_testingFile_dev.jmx" :
            "${env.WORKSPACE}/config_files/jmeter/jmxFiles/sales_testingFile_qa.jmx"
            String assertionResult = sh(script: "sh /home/ubuntu/jmeter/apache-jmeter-5.4.1/bin/jmeter.sh -n -t ${jmxFileForRegressionSuite} -l  ${env.WORKSPACE}/${env.BUILD_NUMBER}/jtlFile/sales_testing.jtl -e -o ${env.WORKSPACE}/${env.BUILD_NUMBER}/testReport", returnStdout: true)
            echo 'Execution of JMeter test script completed..'
            echo "Assertion result: ${assertionResult}"
            List result = assertionResult.split('\n')
            String errorResult = result[-3]
            String pattern = ~/(Err:\s*[1-9])/
            Boolean isAssertionErrored = pattern.matcher(errorResult).find()
            echo "isAssertionErrored: ${isAssertionErrored}"
            sh "sudo cp -a ${env.WORKSPACE}/${env.BUILD_NUMBER}/testReport/. /var/www/html/"
            echo 'View completed report at http://3.7.238.228/'
            if (isAssertionErrored) {
                echo 'Assertions have failed. Build is set to failure.'
                error('Build failed')
            } else {
                echo 'All assertions have passed.'
                echo 'Test script completed successfully.'
            }
        } else {
            echo 'Regression suite not configured for this project.'
        }

        if (enableTryMe == 'YES') {
            def post = new URL(TRY_ME_URL).openConnection()
            def message = '{ "appId": [], "dialogId": "f0b94daa-8985-4014-92b0-aaad3e57b0b0", "type": "app_scheduler", "from": { "id": "105", "name": "Kea", "channelId": "keadesktopconv", "accountId": "88" }, "headers": { "clientMetaData": { "userAgent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36", "os": "Windows", "browser": "Chrome", "device": "Unknown", "os_version": "windows-10", "browser_version": "103.0.0.0", "deviceType": "desktop", "orientation": "landscape" }, "userid": "105", "username": "Kea", "loggedInUser": "kea@purpleslate.in", "accountId": "88", "channelType": "keadesktopconv", "appid": "a6df101eb68414356b7e663b5d049010d" }, "metadata": { "bucketSize": null, "requestType": "", "duplicateEntityValue": "", "choiceSelected": "", "responseType": "", "ignoreNLU": false, "pageSize": 30, "pageNumber": 1, "filters": [], "choices": [], "entities": [], "pageNumberChange": false, "freeText": false, "followUpPromptType": "", "queryCategory": null, "isWakeWord": false }, "spellCheck": true, "isMultiUser": false, "isNewDialogThreadRequired": null, "locale": "en-US" }'
            post.setRequestMethod('POST')
            post.setDoOutput(true)
            post.setRequestProperty('Content-Type', 'application/json')
            post.getOutputStream().write(message.getBytes('UTF-8'))
            def result = post.getResponseCode()
            if (result == (200)) {
                echo post.getInputStream().getText()
            }
        } else {
            echo 'Skipping Try Me query.'
        }
    }

}
// stage('Release: Dev') {
            //     input {
            //         message 'Approve the release for Dev environment'
            //         ok 'Approve'
            //         submitter 'admin'
            //     }
            //     releaseNodeService = new ReleaseNodeService()
            //     releaseNodeService.releaseNodeServices(pipelineParams, dockerImageName, dockerContainerName, 'psKeaDEV')
            // }

            // stage('Release: QA') {
            //     input {
            //         message 'Approve the release for QA environment'
            //         ok 'Approve'
            //         submitter 'admin'
            //     }
            //     releaseNodeService = new ReleaseNodeService()
            //     releaseNodeService.releaseNodeServices(pipelineParams, dockerImageName, dockerContainerName, 'psKeaQA2')
            // }

            // stage('Release: Beta') {
            //     input {
            //         message 'Approve the release for Beta environment'
            //         ok 'Approve'
            //         submitter 'admin'
            //     }
            //     releaseNodeService = new ReleaseNodeService()
            //     releaseNodeService.releaseNodeServices(pipelineParams, dockerImageName, dockerContainerName, 'psKeaBeta')
            // }

